package id.iconpln.listtouristapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnLogin.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        if (checkInput()) {
            if (etUsername.text.toString() == "user@mail.com" && etPassword.text.toString() == "password") {
                val listTouristIntent = Intent(this, MainActivity::class.java)
                startActivity(listTouristIntent)
                finish()
            } else {
                Toast.makeText(this, "username or password wrong", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun checkInput(): Boolean {
        when {
            etUsername.length() == 0 -> {
                etUsername.setError("username kosong")
                return false
            }
            etPassword.length() == 0 -> {
                etPassword.setError("password kosong")
                return false
            }
            !checkEmail(etUsername.text.toString()) -> {
                etUsername.setError("username salah format email")
                return false
            }
            etPassword.length() < 7 -> {
                etPassword.setError("password kurang dari 7")
                return false
            }
            else -> return true
        }
    }

    private fun checkEmail(email: String): Boolean {
        val pattern = Patterns.EMAIL_ADDRESS
        return pattern.matcher(email).matches()
    }
}
