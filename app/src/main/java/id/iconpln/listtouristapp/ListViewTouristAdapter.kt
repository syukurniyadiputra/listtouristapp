package id.iconpln.listtouristapp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import id.iconpln.listtouristapp.model.Tourist

class ListViewTouristAdapter(val context: Context, val listTourist: ArrayList<Tourist>): BaseAdapter() {

    override fun getView(index: Int, view: View?, viewGroup: ViewGroup?): View {
        val viewLayout = LayoutInflater.from(context)
            .inflate(R.layout.item_list_tourist, viewGroup, false)

        val viewHolder = ViewHolder(viewLayout)
        val tourist = getItem(index) as Tourist
        viewHolder.bind(context, tourist)

        return viewLayout
    }

    override fun getItem(index: Int): Any {
        return listTourist[index]
    }

    override fun getItemId(index: Int): Long {
        return index.toLong()
    }

    override fun getCount(): Int {
        return listTourist.size
    }

    private inner class ViewHolder(view: View) {
        private val tvTouristName: TextView = view.findViewById(R.id.tv_tourist_name)
        private val tvTouristDesc: TextView = view.findViewById(R.id.tv_tourist_desc)
        private val ivTouristPhoto: ImageView = view.findViewById(R.id.iv_tourist_image)

        fun bind(context: Context, tourist: Tourist) {
            tvTouristName.text = tourist.name
            tvTouristDesc.text = tourist.desc

            Glide.with(context)
                .load(tourist.photo)
                .into(ivTouristPhoto)
        }
    }
}