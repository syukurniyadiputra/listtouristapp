package id.iconpln.listtouristapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import id.iconpln.listtouristapp.model.Tourist
import kotlinx.android.synthetic.main.activity_detail_tourist.*

class DetailTouristActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        const val EXTRA_TOURIST = "extra_tourist"
    }

    lateinit private var tourist: Tourist

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_tourist)

        setupActionBar()
        initIntentExtras()
        displayTouristDetail()
        setOnclickButton()
    }

    private fun setupActionBar() {
        supportActionBar?.title = "Detail Tourist"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    private fun initIntentExtras() {
        tourist = intent.getParcelableExtra(EXTRA_TOURIST)
    }

    private fun displayTouristDetail() {
        tvTouristDetailName.text = tourist.name
        tvTouristDetailDesc.text = tourist.desc
        tvTouristRegion.text = tourist.region
        Glide.with(this)
            .load(tourist.photo)
            .apply(
                RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.ic_launcher_foreground)
                    .error(R.drawable.ic_launcher_foreground)
            )
            .into(ivTouristDetailImage)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> {
                false
            }
        }
    }

    private fun setOnclickButton() {
        btnTouristShare.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnTouristShare -> {
                val shareTouristIntent = Intent(Intent.ACTION_SEND)
                val titleAndContent = "${tourist.name}\n\n${tourist.desc}"
                shareTouristIntent.putExtra(Intent.EXTRA_TEXT, titleAndContent)
                shareTouristIntent.type = "text/plain"

                val shareIntent = Intent.createChooser(shareTouristIntent,"Pilih Layanan untuk share")
                if (shareIntent.resolveActivity(packageManager) != null) {
                    startActivity(shareIntent)
                }
            }
        }
    }
}
