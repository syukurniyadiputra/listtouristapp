package id.iconpln.listtouristapp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Tourist (
    var name: String? = "",
    var desc: String? = "",
    var photo: String? = "",
    var region: String? = ""
): Parcelable

