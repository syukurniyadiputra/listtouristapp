package id.iconpln.listtouristapp

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import id.iconpln.listtouristapp.model.Tourist
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private val listTourist: ListView
        get() = lv_list_tourist

    var list_of_region = arrayOf(
        "Pilih Berdasarkan Daerah",
        "Kota Yogyakarta",
        "Kabupaten Bantul",
        "Kabupaten Gunung Kidul",
        "Kabupaten Kulon Progo",
        "Kabupaten Sleman")

    var spinner:Spinner? = null

    private var region: String = ""

    private var list: ArrayList<Tourist> = arrayListOf()

    private var listSort: ArrayList<Tourist> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        spinner = this.sort_region
        spinner!!.setOnItemSelectedListener(this)

        val region = ArrayAdapter(this, android.R.layout.simple_spinner_item, list_of_region)
        region.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner!!.setAdapter(region)

        setListClickListener(listTourist)
    }

    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {
        region = list_of_region[position]
        loadListSortBaseAdapter(this)
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    private fun setListClickListener(listView: ListView) {
        listView.onItemClickListener = object : AdapterView.OnItemClickListener {
            override fun onItemClick(adapter: AdapterView<*>?, view: View?, index: Int, l: Long) {
                Toast.makeText(this@MainActivity, "${listSort[index].name}", Toast.LENGTH_SHORT).show()
                showDetailTourist(listSort[index])
            }
        }
    }

    private fun showDetailTourist(tourist: Tourist) {
        val detailTouristIntent = Intent(this, DetailTouristActivity::class.java)
        detailTouristIntent.putExtra(DetailTouristActivity.EXTRA_TOURIST, tourist)
        startActivity(detailTouristIntent)
    }

    private fun loadListSortBaseAdapter(context: Context) {
        loadListView()
        for(element in list) {
            if (region == "Pilih Berdasarkan Daerah") {
                listSort.add(element)
            } else if (element.region == region && region != "Pilih Berdasarkan Daerah") {
                listSort.add(element)
            }
        }

        val baseAdapter = ListViewTouristAdapter(context, listSort)
        listTourist.adapter = baseAdapter
    }

    private fun loadListView() {
        list.clear()
        listSort.clear()
        listTourist.deferNotifyDataSetChanged()
        listTourist.adapter = null
        list.addAll(TouristData.listDataTourist)
    }
}
