package id.iconpln.listtouristapp

import id.iconpln.listtouristapp.model.Tourist

object TouristData {

    val listDataTourist: ArrayList<Tourist>
        get() {
            // create empty list Tourist
            val list = ArrayList<Tourist>()
            for(data in dataTourist) {
                val tourist = Tourist()
                tourist.name = data[0]
                tourist.desc = data[1]
                tourist.photo = data[2]
                tourist.region = data[3]
                list.add(tourist)
            }
            return list
        }

    private var dataTourist = arrayOf(
        arrayOf(
            "Taman Sari Jogja",
            "Jaman dulu Taman Sari Jogja merupakan tempat rekreasi dan meditasi keluarga kerajaan Yogyakarta. Pesona Istana Taman Sari terletak pada keindahan arsitekturnya yang kuno dan pemandangan yang indah.",
            "https://www.nativeindonesia.com/wp-content/uploads/2018/05/istana-tamansari.jpg",
            "Kota Yogyakarta"
        ),
        arrayOf(
            "Candi Prambanan",
            "Candi lainnya yang terkenal adalah Candi Prambanan. Terletak sekitar 17Km dari pusat kota Yogyakarta, Candi Prambanan merupakan candi Hindu terbesar di Indonesia. Candi ini memiliki relief yang menceritakan kisah mengenai Ramayana dan Krishnayana.",
            "https://www.nativeindonesia.com/wp-content/uploads/2018/05/candi-prambanan.jpg",
            "Kabupaten Sleman"
        ),
        arrayOf(
            "Pantai Indrayanti",
            "Pantai Indrayanti adalah salah satu destinasi wisata pantai di Jogja yang sekarang cukup populer. Dengan bentang pasir yang putih bersih khas pantai selatan, pemandangan yang indah, dan fasilitas yang cukup lengkap, pantai ini banyak menjadi tujuan wisatawan.",
            "https://www.nativeindonesia.com/wp-content/uploads/2017/07/pantai-sawal-indrayanti.jpg",
            "Kabupaten Gunung Kidul"
        ),
        arrayOf(
            "Pantai Nglambor",
            "Di Yogya juga terdapat pantai yang bisa untuk snorkeling asyik lho, tempatnya di pantai nglambor. Tidak seperti pantai selatan lainnya, disini terdapat laguna dengan air yang cukup tenang. Kekayaan alam disini bisa kita nikmati sambil snorkeling.",
            "https://www.nativeindonesia.com/wp-content/uploads/2017/05/jembatan-pulau-nglambor.jpg",
            "Kabupaten Gunung Kidul"
        ),
        arrayOf(
            "Pantai Pok Tunggal",
            "Satu lagi pantai yang perlu anda kunjungi di Jogja, pantai pok tunggal. Keindahan pantai ini tidak kalah dengan pantai lainnya di Yogyakarta. Fasilitasnya memang belum selengkap tempat lain, tapi sepadan dengan pemandangan yang bisa anda dapatkan.",
            "https://www.nativeindonesia.com/wp-content/uploads/2015/04/pantai-pok-tunggal-gunung-kidul-yogyakarta.jpg",
            "Kabupaten Gunung Kidul"
        ),
        arrayOf(
            "Jalan Malioboro",
            "Jalan paling terkenal di Yogyakarta adalah Jalan Malioboro. Di sepanjang jalan ini terdapat toko-toko dan pedagang kaki lima yang menjual berbagai macam barang, dari pakaian, kerajinan tangan sampai makanan. Salah satu hal yang khas mengenai Malioboro adalah penjual makanan lesehan.",
            "https://www.nativeindonesia.com/wp-content/uploads/2018/05/jalan-malioboro.jpg",
            "Kota Yogyakarta"
        ),
        arrayOf(
            "Pasar Beringharjo",
            "Pasar Beringharjo terletak di Jalan Malioboro. Pasar tertua di Yogyakarta ini terkenal sebagai tempat turis membeli souvenir dengan harga murah. Berbagai macam barang tersedia di pasar ini, dari batik, makanan tradisional, uang kuno, bahan dasar jamu tradisional hingga barang antik.",
            "https://www.nativeindonesia.com/wp-content/uploads/2018/05/pasar-beringharjo.jpg",
            "Kota Yogyakarta"
        ),
        arrayOf(
            "Benteng Vredeburg",
            "Satu lagi tempat wisata di Jogja dekat Malioboro. Benteng Vredeburg juga terletak di Jalan Malioboro, tepatnya di depan Gedung Agung. Benteng yang kini telah menjadi museum yang berisi diorama mengenai sejarah Indonesia itu sangat cocok dikunjungi bagi Anda yang ingin menambah wawasan tentang sejarah Indonesia.",
            "https://www.nativeindonesia.com/wp-content/uploads/2018/05/benteng-vredeburg.jpg",
            "Kota Yogyakarta"
        ),
        arrayOf(
            "Museum Ullen Sentalu",
            "Museum Ullen Sentalu terletak di Jalan Boyong Km 25 Kaliurang Barat, Sleman,Yogyakarta. Tempat wisata Sleman ini dibangun diatas area seluas 1,2 hektar dengan suhu udara sekitar berkisar antara 15-20 derajat celcius. Arsitektur museum ini sangat unik, jalan masuk ke dalam museum sendiri terdiri dari undakan, kelokan dan labirin yang menuju ke salah satu ruang pameran yang berada di bawah tanah.",
            "https://www.nativeindonesia.com/wp-content/uploads/2018/05/museum-ullen-sentalu.jpg",
            "Kabupaten Sleman"
        ),
        arrayOf(
            "Keraton Yogyakarta",
            "Salah satu tempat wisata budaya lainnya di Yogyakarta adalah Keraton Yogyakarta. Keraton ini adalah salah satu bangunan bersejarah kesultanan Yogyakarta yang ditempati oleh Sultan dan keluarga Sultan. Di keraton ini juga terdapat museum yang memamerkan barang-barang kesultanan Yogyakarta dari barang rumah tangga sampai barang-barang unik yang sebagian merupakan hadiah dari raja Eropa.",
            "https://www.nativeindonesia.com/wp-content/uploads/2018/05/keraton-yogyakarta.jpg",
            "Kota Yogyakarta"
        )
    )
}